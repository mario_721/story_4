from django.shortcuts import render
from .forms import UserModelForm
from .models import jadwalkua
from django.http import HttpResponseRedirect

# Create your views here.
def index(request):
    return render(request, 'index.html')

def my_story(request):
	return render(request, 'my_story.html')

def jadwalku(request):
    data = jadwalkua.objects.all()
    context = {}
    if request.method == 'POST':
        
        form = UserModelForm(request.POST or None)
        if form.is_valid():
            form.save()
            form = UserModelForm()
            return HttpResponseRedirect('/jadwalku/')
    else:
        form = UserModelForm()


    return render(request, 'jadwalku.html',{'form': form,"data":data})

def delete(request, id):
    data = jadwalkua.objects.get(id=id)
    data.delete()
    return HttpResponseRedirect('/jadwalku/')
