from django.db import models

# Create your models here.

class jadwalkua(models.Model):
    # Fields
    date_field = models.DateField()
    activity_field = models.CharField(max_length=50)
    venue_field = models.CharField(max_length=50)
    category_field = models.CharField(max_length=50)

    # Methods
    
    def __str__(self):
        """String for representing the MyModelName object (in Admin site etc.)."""
        return self.activity_field
