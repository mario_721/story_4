from django import forms
from .models import jadwalkua


class UserModelForm(forms.ModelForm):
    class Meta:
        model = jadwalkua
        fields = '__all__'
