from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('my_story/', views.my_story, name='my_story'),
    path('jadwalku/', views.jadwalku, name='jadwalku'),
    path('delete/<id>', views.delete, name='delete')

    # dilanjutkan ...
]
